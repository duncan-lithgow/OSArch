# Suggested agenda
   * Possible agenda topics can be added to Etherpad https://scalar.vector.im/etherpad/p/!TXapfsdaYQHivXBBEN_matrix.org

# Meeting notes for yyyy-mm-dd

## Fixed agenda
   1. Who chairs the meeting? [name]
   1. Who takes minutes? [name]
   1. Follow up on previous decisions: OSArch Open Collective has been pushed to SoMe
   1. Decide agenda

## Quick topics
   * ...

## Main Topics

### Topic

# Possible future agenda items
   * Month:

# Host/organiser rotation list:
   - October: Bruno Postle
   - November: Peter (cadgiru)
   - December: Duncan
   - January: Jesusbill
First year over - call for new members?
   - February: Moult
   - March: Ryan

Host responsibility
    * Ask for & suggest agenda items
    * Check previous agenda for issues that need follow up
    * Make sure someone takes notes
    * Make sure we check all the agenda topics
