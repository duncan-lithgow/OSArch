### 20220226 - OSArch Steering Committee Meeting


---
Housekeeping

- how (much) do we share these meetings? Recording, notes ... ? 
  **Will be recorded**
- how long is this meeting? **1 hour**

### Possible topics:

- introduce yourself \& why we're each here (education/skills/motivation/job)
- how do we want to manage the meetings themselves?
  - **Rotating chair Alphabetical**:
    - **Bruno Postle (4/1)**
    - **cadgiru (5/1)**
    - **Duncan (6/1)**
    - **Jesusbill (7/1)**
    - **Moult (8/1)**
    - **Ryan (9/1)**
 - how do we make a prioritized list **~was not really answered~**
 - how often should we meet, **first Saturday 20:00 UTC**
 - how do we want to make decisions? **Loose verbal voting, at this time.**
          
  
  ### Background notes

Fiscal Hosts

- European / American / Cayman Islands ... where should we 'incorporate'?
  - US
  - Eu
- [https://www.oscollective.org/](https://www.oscollective.org/) on [https://opencollective.com/](https://opencollective.com/)... is that what we want to do?
  - pros
    - we can set it up very easy, can use the one Thomas \& Dion already have.
  - cons: 
    - 10% fee
    - low advocacy
    - More Open Source than Free Software
- ~~Software Conservancy~~
  - ~~pro-active advocacy and license enforcement~~
  - ~~more Free Software than Open Source~~
- chat about 501(c)(3) non-profit status...want to pursue? OpenCollective collectives are 501(c)(3)
- - 501(c)(3), What is that ([https://en.wikipedia.org/wiki/501(c)\_organization)?](https://en.wikipedia.org/wiki/501(c)\_organization)?)
	  - To be tax-exempt under section 501(c)(3) of the Internal Revenue Code, an organization must be organized and operated exclusively for exempt purposes set forth in section 501(c)(3), and none of its earnings may inure to any private shareholder or individual.
  - Pros
    - From chatter on OSArch forum, this seems the easiest option for corporations to donate to. 
  - Cons
    - Somewhat expensive to initiate, but it seems doable 
  - Links in the wild
    - how much it costs: [https://aaronhall.com/how-much-does-it-cost-to-set-up-a-501c3-nonprofit-organization/](https://aaronhall.com/how-much-does-it-cost-to-set-up-a-501c3-nonprofit-organization/)
    - could be cheaper option.. [https://www.legalzoom.com/marketing/business-formation/nonprofit](https://www.legalzoom.com/marketing/business-formation/nonprofit)
 - **Action items**
	 - **Ryan will research 501c**
	 - **Ioannis will research European equivalent**
	 - **Discussion here: https://community.osarch.org/discussion/916/research-on-best-legal-entity-for-osarch#latest**



---

### Random notes, unable to categorize

- Dion: maybe a company (or foundation?) is clearer for other companies to understand?

Funding
- Look at it from what's in it for me perspective

---

### Key
- **bold**: decided and/or action items
- ~~strikeout~~: not discussed in meeting

---

*Unorganized notes taken during meeting commented out below, see markdown code...*

<!--


Maybe first issue

* how (much) do we share these meetings? Recording, notes ... ? Will be recorded
* how long is this meeting? 1 hour

### Possible topics:

       * introduce yourself \& why we're each here (education/skills/motivation/job)
       * how do we want to manage the meetings themselves? Rotating chair ... ? Alphabetical: Bruno Postle, cadgiru, Duncan, Jesusbill, Moult, Ryan
       * how do we make a prioritized list
       * Fiscal host (see notes)
       * how often should we meet, first Saturday 20:00 UTC
       * how do we want to make decisions?

Dions todo list:

*  whoever thinks they can set up a company (Ryan) research options. what, when how?

Fiscal hosts

* Dion: maybe a company (or foundation?) is clearer for other companies to understand?
* 501(c)(3), What is that ([https://en.wikipedia.org/wiki/501(c)\_organization)?](https://en.wikipedia.org/wiki/501(c)\_organization)?)
  * 3 To be tax-exempt under section 501(c)(3) of the Internal Revenue Code, **an organization must be organized and operated exclusively for exempt purposes set forth in section 501(c)(3), and none of its earnings may inure to any private shareholder or individual**.
  * 5 ?

Funding

* Look at it from what's in it for me perspective
  
  ### Background notes

Fiscal Hosts

* European / American / Cayman Islands ... where should we 'incorporate'?
  * US
  * Eu
* [https://www.oscollective.org/](https://www.oscollective.org/) on [https://opencollective.com/](https://opencollective.com/)... is that what we want to do?
  * pros
    * we can set it up very easy, can use the one Thomas \& Dion already have.
  * cons: 
    * 10% fee
    * low advocacy
    * More Open Source than Free Software
* Software Conservancy
  * pro-active advocacy and license enforcement
  * more Free Software than Open Source
* chat about 501(c)(3) non-profit status...want to pursue? Duncan notes: OpenCollective collectives are 501(c)(3)
  * Pros
    * From chatter on OSArch forum, this seems the easiest option for corporations to donate to. 
  * Cons
    * Somewhat expensive to initiate, but it seems doable 
  * Links in the wild
    * how much it costs: [https://aaronhall.com/how-much-does-it-cost-to-set-up-a-501c3-nonprofit-organization/](https://aaronhall.com/how-much-does-it-cost-to-set-up-a-501c3-nonprofit-organization/)
    * could be cheaper option.. [https://www.legalzoom.com/marketing/business-formation/nonprofit](https://www.legalzoom.com/marketing/business-formation/nonprofit)

### Next meeting(s):

* ...


-->
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTM1NDMxODEzNCwyMTUyNTYwMjcsNTc5Nj
I1OTg2XX0=
-->