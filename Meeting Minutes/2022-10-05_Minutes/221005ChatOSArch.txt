Moult (with no mic)
Moult (with no mic) says:agreed, need to make the baby steps on spending money 
Moult (with no mic) says:heyhey 
Moult (with no mic) says:i know sorry no mic 😃 left it in the car 
14:09
Ioannis - Jesusbill
Ioannis - Jesusbill says:👍 
14:10
Moult (with no mic)
Moult (with no mic) says:agreed yass' idea is good 😃 
Moult (with no mic) says:he should go for it and we can help advertise it 
Moult (with no mic) says:i don't fully understand the 1$ per month tier 
Moult (with no mic) says:i don't think that's a good idea, it encourages people to lowball donations 
Moult (with no mic) says:the tiers can be set to variable 
Moult (with no mic) says:so they "start" at X, but the user can set to increase 
Moult (with no mic) says:you can, but it's good psychologically to offer things 
Moult (with no mic) says:
https://opencollective.com/opensourcebim
 examples of tiers i setup for opensourcebim 
Moult (with no mic) says:the descriptions also help on the scope of the donation 
Moult (with no mic) says:so do i udnerstand correctly there are only 4 project proposals for funding right now? 
Moult (with no mic) says:indeed i'm trying to find people to spend it on, been searching for devs 
Moult (with no mic) says:finding devs is really hard 
Moult (with no mic) says:well actually there are lots of simple scoped things 
Moult (with no mic) says:like stair generator 
Moult (with no mic) says:or even "build the ios website" 
Moult (with no mic) says:stuff we don't want to spend time on and let someone else do 
Moult (with no mic) says:or "create this graphic for the documentation" 
Moult (with no mic) says:hehehehehe 
Moult (with no mic) says:so that sounds like individual projects, not communities? 
14:21
me says:
https://community.osarch.org/discussion/1166/us-national-science-foundation-pathways-to-enable-open-source-ecosystems#latest
 
14:21
Moult (with no mic)
Moult (with no mic) says:i looked on the website and i have no idea what the process is 
Moult (with no mic) says:Proposals may only be submitted by the following:

    Institutions of Higher Education (IHEs) - Two- and four-year IHEs (including community colleges) accredited in, and having a campus located in the US, 
Moult (with no mic) says:Non-profit, non-academic organizations: Independent museums, observatories, research labs, professional societies and similar organizations in the U.S. associated with educational or research activities. 
Moult (with no mic) says:^ the closest we fit under, but not really since we only use opensourcecollective as an umbrella 
Moult (with no mic) says:so maybe we are not eligible 
Moult (with no mic) says:are we? 
Moult (with no mic) says:opensourcecollective is a non profit, but we aren't 😃 
Moult (with no mic) says:The Project Description can be up to 15 pages. 
Moult (with no mic) says:^ yeouch 
14:26
^ yeouch 
Moult (with no mic) says:Phase I awardees are not obligated to submit Phase II proposals in the future. An NSF POSE Phase I award is not required for the submission of a Phase II proposal. 
Moult (with no mic) says:so did i get that right thereare only now only 4 proposals for funding from osarch? 
14:28
Ioannis - Jesusbill
Ioannis - Jesusbill says:
https://community.osarch.org/discussion/1156/a-call-for-projects-to-fund-via-osarchs-open-collective-site#latest
 
14:29
Moult (with no mic)
Moult (with no mic) says:
https://community.osarch.org/discussion/1156/a-call-for-projects-to-fund-via-osarchs-open-collective-site#latest
 3 from ryan and 1 from bitacovir 
Moult (with no mic) says:it's still on the front page 
14:30
Ryan Schultz (theoryshaw)
Ryan Schultz (theoryshaw) says:
https://community.osarch.org/discussion/1156/a-call-for-projects-to-fund-via-osarchs-open-collective-site#latest
 
14:30
Moult (with no mic)
Moult (with no mic) says:so let's say one of those gets selected, like the ladybug one for example 
Moult (with no mic) says:what next? we post an ad for a developer? 
Moult (with no mic) says:someone needs to guide that dev / audit the code? 
Moult (with no mic) says:it sounds very speculative 
Moult (with no mic) says:there's waiting for people to fund it 
Moult (with no mic) says:then waiting for a dev to act on it 
Moult (with no mic) says:yeah like a bounty 
Moult (with no mic) says:wouldn't it also be good to decide on a nonspeculative thing we can spend money on? 
Moult (with no mic) says:some small amounts, just to get  into the swing of things 
Moult (with no mic) says:ioannis you're on mute 
Moult (with no mic) says:yep 
Moult (with no mic) says:yep 
Moult (with no mic) says:and i think freecad also has sponsored devs now let me check 
Moult (with no mic) says:not saying we decide privately 
Moult (with no mic) says:
https://github.com/FreeCAD/FPA#readme
Moult (with no mic) says:
https://github.com/FreeCAD/FPA/issues
Moult (with no mic) says:there's 8 possible projects 
Moult (with no mic) says:publishing freecad on the apple store is 100bucks a year for example 
Moult (with no mic) says:what i'm suggesting is that the current approach of bounty projects is very speculative 
Moult (with no mic) says:i think we should find a small concrete thing that we can start sponsoring sooner rather than later 
Moult (with no mic) says:get some real results, even if the amount is financially low 
Moult (with no mic) says:that's fine, but those 2 projects could take very, very long to actually happen 
Moult (with no mic) says:since the scope in ambiguous, and there is potentially not even a dev available 
Moult (with no mic) says:yeah, we  need something to show 
Moult (with no mic) says:"yes we don't have much money, but with what we have, we've made a difference with what we can afford" 
Moult (with no mic) says:there are 4 currently 
Moult (with no mic) says:the idea is still good 
Moult (with no mic) says:but we need to work on making the proposals very concrete 
Moult (with no mic) says:agreed, keep the community process 
Moult (with no mic) says:right now 1) projects don't have a deadline 2) projects may not have a dev available 3) projects don't have a budget/money requirement 
Moult (with no mic) says:yeah, very concrete, give me $X and in return {foo} will produce - feature A, - feature B - feature C 
yeah someone needs to own it 
Moult (with no mic) says:they can't just say "i wish project foo did this" 
just like a grant submission, you don't say "i wish this happened", you say "i'll make this happen and i need $X" 
duncan
duncan says:"they can't just say "i wish project foo did this" exactly this. a list of aspirational ideas is not very useful. it's more of a five year strategy statement. 
duncan says:I think we need to focus the submissions *before* we submit them for a vote. 
Moult (with no mic)
Moult (with no mic) says:maybe a good way to make it more concrete is "we'll fund $1,000 dollars". what will you give us? 
s/fund/fund up to/ 
agreed, it's a lot safer bet 
Moult (with no mic) says:the devs are already doing it after all 
Moult (with no mic) says:kinda like an epic megagrant ... osarch grant 😃 
Moult (with no mic) says:i'm happy to talk to yorik 
Moult (with no mic) says:or wassim, say what if he could sponsor a student to work over a holiday like a mini gsoc 
duncan
duncan says:I'll write t oYorik. Who would like to write to Wassim? If the result is a project porposal - great! if not, that's also fine. Who else could we write to? Some people only need a gently push. 
Bruno
Bruno says:I can write to Wassim. I'll just ask if there is any scope for spending 500 or 1000 
14:57
Moult (with no mic)
Moult (with no mic) says:he froze? 
agreed 
Moult (with no mic) says:and we should clearly state how much we could fund them. i think up to 1k is a sensible upper limit given our capacity of raising funds 
Moult (with no mic) says:i know originally it was like a fundraising project on opencollective which could be any amount 
Moult (with no mic) says:but without a dollar value it's hard to know the size of projects 
15:01
they can tell us, but we can't promise 100k 😃 
i think we're agreeing 
Moult (with no mic) says:the point is a number 
Moult (with no mic) says:yes that would be good 
Moult (with no mic) says:because right now "A Call for Projects to Fund" but the funding amount is unknown is weird 
Moult (with no mic) says:sorry if i've added so much confusion 
Moult (with no mic) says:i just really wanted to see a concrete thing achieved and paid for by osarch sooner rather than later 😃 
yep 
Moult (with no mic) says:so sorry 
duncan said he's ping yorik 

