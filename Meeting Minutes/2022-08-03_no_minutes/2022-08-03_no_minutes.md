# Suggested agenda
   * Possible agenda topis are on Etherpad https://scalar.vector.im/etherpad/p/!TXapfsdaYQHivXBBEN_matrix.org_Test_page

# Meeting notes for Day, Month, Time

## Fixed agenda
   1. Who chairs the meeting? (doesn't have to be the organizer)
   1. Who takes minues? (should not be the chair)
   1. Follow up on previous decisions
   1. Decide agenda

## Quick topics
   * ...

## Main Topics
   * ...

### ...

# Possible future agenda items
   * ...

Next meeting is hosted by **Moult**.

Host rotation list:
   - July: Jesusbill
   - August: Moult
   - September: Ryan
   - October: Bruno Postle
   - November: Peter (cadgiru)
   - December: Duncan
   - January: Jesusbill
First year over - call for new members?
   - February: Moult
   - March: Ryan


