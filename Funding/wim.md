# Funding
Why do we need funding?
What should funding be used for?
How should this be distributed?




# Stakeholders
Try to categorize the stakeholders which show an interest in OSArsch

- Institutional 
- Medium - Large companies
- Smaller companies
- Academia
- professional or private Individuals



# WIIFM
Starting with the mission statement of OSArch

**We help create the built environment with free software, increased transparency, and a more ethical approach.**


Looking from a What's in it for me perspective, meaning who stands to benefit commercially and ethically from this mission statement ?
## Institutional
Category includes a diverse body of publicly funded entities. Among these are
- Standards bodies
  - International
  - National
- National building commissioners, property managers and developers
- Innovation bodies
  - Not sure how to categorize these, so use an example from Norway. Innovasjon Norge

Looking at the mission statement of OSArch, this category might consist of the ones with most to gain from advancing a Standards based and ethically sound approach .

Why?

They :
- seldom do any design authoring, but receive documentation from very disparate sources.
- Often responsible for implementing BEM’s with specific requirements 
- Need to preserve data for a long period of time, typically 30Y+
- Stand to benefit greatly from avoiding vendor lock-in
- Are often responsible for documentation over the whole lifeCycle
- benefit from increased transparency and a more ethical approach.


## Major commercial players
This category will probably look at the OSArch initiative from a pure commercial view.
Many of the OSArch contributors are employed by, and indirectly funded, being able to contribute during work time. 

Believe their benefit from contributing would be:
- Good for public relations
- Maybe commercially sound
  - many would say.. This is accessible to us whether we spend money or not.
    - Why should we when our competitors reap the same benefit without financially supporting 

## Smaller companies



## Private individuals
Often engage in OSArch out of
- Curiosity
- Idealism
- Learning

Many great contributors among these. They mostly engage on a ProBono basis.
## Academia
Great for promoting OSArch values among students, and also a large contributor to OpenSource initiatives.
Probably not the ones with the largest financial resources

# How to attract partners
## Institutional
Believe in order to attract these types of partners, we need a professional personal approach.
- White papers
- Seminars
- Presentations
  - Dion
  - Yorik
  - Others
- Links to 
  - Web
  - Social media

### What’s OSArch missing today

What are we missing today in order to attract these partners?
- Structured organization
- Structured teams with specific goals
- Transparency regarding funding of projects, teams or individuals

[Lots of good thoughts in this post](https://community.osarch.org/discussion/182/organizational-structure-and-governance-of-osarch), liked the ideas in post on Jan 23rd by MaartenFrough



## Regarding presentations / white papers
Have recently showed videos to people within the Building and construction industry. 
They have not been particularly versed in  IT, but with considerable experience and strength.

When showing the video presentations, they ‘fall off’ when we get into the technical stuff. What they have liked is the Open Standards approach.



